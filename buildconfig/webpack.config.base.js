/* eslint-disable @typescript-eslint/no-var-requires */
const { DefinePlugin, EnvironmentPlugin } = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const Dotenv = require('dotenv-webpack');

require('dotenv').config({ path: path.join(__dirname, '../.env') });

module.exports = {
  entry: './src/index.tsx',
  output: {
    path: path.join(__dirname, '../dist'),
    filename: '[name].bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack', 'url-loader'],
      },
      {
        test: /\.(jpe?g|png|gif)$/,
        use: ['file-loader'],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({ template: './src/index.html' }),
    new DefinePlugin({
      ENV_PRODUCTION: JSON.stringify(process.env.ENV_PRODUCTION === 'true'),
    }),
    new CopyPlugin([{ from: './src/style.css', to: '' }]),
    new Dotenv({ path: path.join(__dirname, '../.env') }),
  ],
  resolve: {
    modules: ['src', 'node_modules'],
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json', '*'],
  },
};
