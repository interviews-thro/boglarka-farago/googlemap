/* eslint-disable @typescript-eslint/no-var-requires */
const merge = require('webpack-merge');
const baseConfig = require('./webpack.config.base');

module.exports = merge(baseConfig, {
  mode: 'development',
  devServer: {
    port: process.env.PORT,
    historyApiFallback: true,
    hot: true
  },
  devtool: 'source-map',
});
