/* eslint-disable @typescript-eslint/no-var-requires */
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const OfflinePlugin = require('offline-plugin');

const baseConfig = require('./webpack.config.base');

const { AUTOUPDATE_INTERVAL } = process.env;
const SECOND = 1000;
const autoUpdate = (AUTOUPDATE_INTERVAL ? parseInt(AUTOUPDATE_INTERVAL, 10) : 60) * SECOND;

module.exports = merge(baseConfig, {
  mode: 'production',
  plugins: [
    new CleanWebpackPlugin(),
    new OfflinePlugin({
      autoUpdate,
      ServiceWorker: {
        events: true,
      },
    }),
  ],
});
