import React from 'react';
import BootstrapComponent from './bootstrap';
import AppRoutes from './Routes';

const App: React.FC = () => (
  <BootstrapComponent>
    <AppRoutes />
  </BootstrapComponent>
);

export default App;
