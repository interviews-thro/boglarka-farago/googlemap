import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';

import Home from './containers/Home';
import NotFound from './containers/NotFound';

const AppRoutes: React.FC = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/notfound" element={<NotFound />} />
      <Route path="*" element={<Navigate to="/notfound" />} />
    </Routes>
  );
};

export default AppRoutes;
