import { Theme } from '@mui/material';
import React from 'react';

declare module '@mui/material/styles/createTheme' {
  interface Theme {
    appDrawer: {
      width: React.CSSProperties['width'];
      iconSize: React.CSSProperties['width'];
    };
    panel: {
      leftWidth: React.CSSProperties['width'];
      rightWidth: React.CSSProperties['width'];
    };
    header: {
      height: React.CSSProperties['height'];
    };
  }

  // allow configuration using `createMuiTheme`
  interface ThemeOptions {
    appDrawer?: {
      width?: React.CSSProperties['width'];
      iconSize?: React.CSSProperties['width'];
    };
    panel?: {
      leftWidth?: React.CSSProperties['width'];
      rightWidth?: React.CSSProperties['width'];
    };

    header?: {
      height?: React.CSSProperties['height'];
    };
  }
}
