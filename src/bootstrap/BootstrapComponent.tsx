import React, { Suspense } from 'react';
import { CssBaseline } from '@mui/material';
import { ThemeProvider as MuiThemeProvider } from 'styled-components';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'mobx-react';

import { createStores } from 'stores';
import DefaultErrorBoundary from 'utils/DefaultErrorBoundary';
import theme from './theme';
import LoadingScreen from './LoadingScreen';

const BootstrapComponent: React.FC = ({ children }) => (
  <DefaultErrorBoundary>
    <Provider {...createStores()}>
      <>
        <CssBaseline />
        <MuiThemeProvider theme={theme}>
          <Suspense fallback={<LoadingScreen />}>
            <Router>{children}</Router>
          </Suspense>
        </MuiThemeProvider>
      </>
    </Provider>
  </DefaultErrorBoundary>
);

export default BootstrapComponent;
