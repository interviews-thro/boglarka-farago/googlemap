import React from 'react';
import { Box, LinearProgress, styled, Typography } from '@mui/material';

const Page = styled(Box)({
  width: '100%',
  height: '100vh',
  '& .MuiTypography-root': {
    margin: '5vh 5vw',
  },
});

function LoadingScreen() {
  return (
    <Page>
      <LinearProgress />
      <Typography variant="h1" component="h1">
        Loading...
      </Typography>
    </Page>
  );
}

export default LoadingScreen;
