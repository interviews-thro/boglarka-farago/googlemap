import createTheme, { ThemeOptions } from '@mui/material/styles/createTheme';
import './AugmentThemeOptions';

export function createAppTheme(options?: ThemeOptions) {
  return createTheme({
    appDrawer: {
      width: 225,
    },
    typography: {},
    ...(options || {}),
  });
}

export default createAppTheme({});
