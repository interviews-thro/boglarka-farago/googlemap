import React from 'react';
import Button, { ButtonProps } from '@mui/material/Button';
import { Link, LinkProps } from 'react-router-dom';

export interface ButtonLinkProps extends ButtonProps {
  to: LinkProps['to'];
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const RouteLink = (props: any) => <Link {...props} />;
const ButtonLinkComponent: React.FC<ButtonLinkProps> = (props) => <Button {...props} component={RouteLink} />;

export default ButtonLinkComponent;
