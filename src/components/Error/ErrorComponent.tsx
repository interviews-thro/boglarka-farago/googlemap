import React from 'react';
import { Button, Card, CardContent, Typography, CardActions } from '@mui/material';

const reload = () => {
  window.location.reload();
};

const ErrorComponent: React.FC = () => (
  <Card
    sx={{
      margin: 'auto',
      maxWidth: '600px',
    }}
  >
    <CardContent>
      <Typography variant="h3" component="h1" gutterBottom>
        Something went wrong.
      </Typography>
      <Typography variant="body1">
        We apologize for the inconvenience. Problems are reported automatically and we will try to resolve it as soon as
        possible.
      </Typography>
    </CardContent>
    <CardActions>
      <Button size="small" onClick={reload}>
        Reload the page
      </Button>
    </CardActions>
  </Card>
);

export default ErrorComponent;
