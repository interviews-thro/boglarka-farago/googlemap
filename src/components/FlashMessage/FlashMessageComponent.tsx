import React from 'react';
import { useStores } from 'stores';
import { Snackbar, IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

const FlashMessageComponent: React.FC = () => {
  const { contextStore } = useStores();

  return (
    <>
      {contextStore.flashMessages.map((message) => (
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          key={message.id}
          open={message.open}
          autoHideDuration={message.duration}
          onClose={message.close}
          onExited={() => contextStore.removeMessage(message.id)}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={message.content}
          action={[
            <IconButton key="close" aria-label="Close" color="inherit" onClick={message.close}>
              <CloseIcon />
            </IconButton>,
          ]}
        />
      ))}
    </>
  );
};

export default FlashMessageComponent;
