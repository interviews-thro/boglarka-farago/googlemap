import React from 'react';
import { AppBar, Toolbar } from '@mui/material';
import ButtonLink from 'components/ButtonLink';
//import { useStores } from 'stores';

interface Props {
  destination: string;
  label: string;
}

const HeaderComponent: React.FC<Props> = ({ destination, label }) => {
  return (
    <AppBar position="static">
      <Toolbar>
        <ButtonLink color="inherit" to={destination}>
          {label}
        </ButtonLink>
      </Toolbar>
    </AppBar>
  );
};

export default HeaderComponent;
