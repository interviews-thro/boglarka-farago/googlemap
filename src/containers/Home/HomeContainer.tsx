import React from 'react';
import { Typography, Grid, Box, Card } from '@mui/material';
//import EmptyContainer from 'layouts/Empty';
import MainContainer from 'layouts/Main';
import Css from 'utils/css.type';

const BoxProps: Css = {
  textAlign: 'center',
  paddingTop: '30px',
};

const HomeContainer: React.FC = () => (
  <MainContainer destination="/" label="Back">
    <Box component="div" sx={BoxProps}>
      <Card raised={false}>
        <Typography variant="body1">Valami</Typography>
      </Card>
    </Box>
  </MainContainer>
);

export default HomeContainer;
