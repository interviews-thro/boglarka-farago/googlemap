import React from 'react';
import { Typography, Grid, Box, Card } from '@mui/material';
//import EmptyContainer from 'layouts/Empty';
import MainContainer from 'layouts/Main';

const NotFoundContainer: React.FC = () => (
  <MainContainer destination="/" label="Back">
    <Grid   container spacing={2}>
      <Grid item xs={2}></Grid>
      <Grid item xs={8}>
        <Box>
          <Card sx={{ paddingTop: '30px' }}>
            <Typography variant="h1">404 - Not Found</Typography>
          </Card>
        </Box>
      </Grid>
      <Grid item xs={2}></Grid>
    </Grid>
  </MainContainer>
);

export default NotFoundContainer;
