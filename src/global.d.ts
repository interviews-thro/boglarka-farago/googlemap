// This is not a global but a value inserted by webpack
// Declaring it here as a global so the typescript compiler accepts it
declare const ENV_PRODUCTION: boolean;
