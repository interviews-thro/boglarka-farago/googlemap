import React from 'react';
import DefaultErrorBoundary from 'utils/DefaultErrorBoundary';

import FlashMessageComponent from 'components/FlashMessage';

const EmptyContainer: React.FC = ({ children }) => (
  <DefaultErrorBoundary>
    <FlashMessageComponent />
    {children}
  </DefaultErrorBoundary>
);

export default EmptyContainer;
