import React from 'react';
import DefaultErrorBoundary from 'utils/DefaultErrorBoundary';

import FlashMessageComponent from 'components/FlashMessage';
import HeaderComponent from 'components/Header';

interface HeaderProps {
  destination: string;
  label: string;
  children: any;
}

const MainContainer: React.FC<HeaderProps> = ({ destination, label, children }) => (
  <DefaultErrorBoundary>
    <HeaderComponent destination={destination} label={label} />
    <FlashMessageComponent />
    {children}
  </DefaultErrorBoundary>
);

export default MainContainer;
