import { observable, action } from 'mobx';
import FlashMessage from './FlashMessage';

export class ContextStore {
  @observable
  public flashMessages: FlashMessage[] = [];

  @action.bound
  public showMessage(message: FlashMessage): void {
    this.flashMessages.push(message);
  }

  @action.bound
  public removeMessage(id: string): void {
    const index = this.flashMessages.findIndex((element) => element.id === id);
    this.flashMessages.splice(index);
  }
}

export default ContextStore;
