import uuidv4 from 'uuid/v4';
import { observable } from 'mobx';

class FlashMessage {
  public readonly content: string;
  public readonly id: string;
  public readonly duration?: number;
  @observable public open = true;

  public constructor(content: string, duration?: number) {
    this.content = content;
    this.duration = duration;
    this.id = uuidv4();
  }

  public close = () => {
    this.open = false;
  };
}

export default FlashMessage;
