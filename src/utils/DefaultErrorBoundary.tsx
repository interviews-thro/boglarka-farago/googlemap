import React from 'react';
import ErrorComponent from 'components/Error';

export default class DefaultErrorBoundary extends React.Component<Record<string, unknown>, { isError: boolean }> {
  constructor(props: Record<string, unknown>) {
    super(props);
    this.state = {
      isError: false,
    };
  }

  public static getDerivedStateFromError() {
    return { isError: true };
  }

  public render() {
    const { isError } = this.state;
    const { children } = this.props;
    return isError ? <ErrorComponent /> : children;
  }
}
