import { Theme } from '@mui/material/styles/createTheme';
import { SxProps } from '@mui/system';

type Css = SxProps<Theme>;

export default Css;
